# Flutter Base Project

This project is the basis for an application developed in Flutter using the Flutter Modular + MobX architecture.

[Flutter Modular](https://pub.dev/packages/flutter_modular)  
[MobX](https://pub.dev/packages/mobx)

And the following features are included:  

- Device Information  
- Environment segregation  
- Fastlane to build and distribute application
- Firebase Analytics
- Firebase Auth
- Firebase Cloud Message to Push Notifications
- Firebase Crashlytics
- Firebase Distribution
- Firebase Performance
- Firebase Remote Config
- Flutter analyze hook
- Linter rules to Effective Dart guideline
- Notification page
- Themes

# Firebase Project

This project uses a firebase demo project
[Firebase project](https://console.firebase.google.com/u/0/project/fir-demo-project/overview) when running in dev flavor

# Hook

This project uses a Git Hooks Manager called Lefthook. 

To activate, run `./scripts/activate-lefthook.sh`

# Push Notitications

This project is configured to receive push notifications from Firebase Cloud Message.

The project is ready to support two types of notification:

- No redirection
- With redirection to the notification page

This mechanism can be updated as needed by changing the file:

```
lib
⊦ notifications
  ⊦ notifications_service.dart
    ⊦ notificationHandler()
```

To use redirection to the notifications page, it is mandatory to inform the following data in the message:

- click_action
- image_url
- title
- content
- notification_type

# Configurations to environment segregation

This project has already set up a mechanism for segregating environments.

To configure necessary information that differentiates between environments, change the files:

```
configs
⊦ dev.json
⊦ qa.json
⊦ prd.json
```

For the environment segregation to work we need to make some adjustments on the android and ios platforms

## Android

### Manifest and Package

Change all package `com.labpixies.flood` to your project package name in the files:

```
android  
⊦ app
  ⊦ build.gradle
  ⊦ src
    ⊦ debug
      ⊦ AndroidManifest.xml
    ⊦ profile
      ⊦ AndroidManifest.xml
    ⊦ main
      ⊦ AndroidManifest.xml
      ⊦ kotlin
        ⊦ <change folder structure to your package name, ex: com/labpixies/flood>
          ⊦ MainActivity.kt

``` 

### Flavors

Configure your application's resources and google services according to the environment in the files and folders:

```
android
⊦ app
  ⊦ build.gradle
  ⊦ src
    ⊦ dev
      ⊦ google-services.json
      ⊦ res
        ⊦ drawable → Push notifications icon
        ⊦ minimap → App icon
        ⊦ values 
          ⊦ colors.xml → Push notifications icon color
          ⊦ values.xml → App name
    ⊦ qa
      ⊦ google-services.json
      ⊦ res
        ⊦ drawable
        ⊦ minimap
        ⊦ values 
          ⊦ colors.xml
          ⊦ values.xml
    ⊦ prd
      ⊦ google-services.json 
      ⊦ res
        ⊦ drawable
        ⊦ minimap
        ⊦ values 
          ⊦ colors.xml
          ⊦ values.xml
```

## iOS

All flavor configuration on ios is done directly by xcode.

Reference link for configuration [Build flavors in Flutter (Android and iOS) with different Firebase projects per flavor](https://medium.com/@animeshjain/build-flavors-in-flutter-android-and-ios-with-different-firebase-projects-per-flavor-27c5c5dac10b)

### Important

To build in iOS we need run ruby in 2.7.2 version.

Troubleshooting on MacOS when flutter run on ios is breaking:

- brew upgrade ruby
- sudo gem install cocoapods
- cd ios -> pod install

# Running application in specific flavor

- dev → `flutter run --flavor dev -t lib/main.dart`
- qa → `flutter run --flavor qa -t lib/main_qa.dart`
- prd → `flutter run --flavor prd -t lib/main_prd.dart`

# Fastlane

This project uses Fastlane to create and distribute applications. The Fastfile is found at:

### Android  
   
```
android
⊦ fastlane
  ⊦ Fastfile
```

Inside the android folder execute the commands  

- dev → `bundle exec fastlane dev_android_app`  
- qa → `bundle exec fastlane qa_android_app`  
- prd → `bundle exec fastlane prd_android_app`  

### iOS  
   
```
ios
⊦ fastlane
  ⊦ Fastfile
```

Inside the ios folder execute the commands  

- dev → `bundle exec fastlane dev_ios_app`  
- qa → `bundle exec fastlane qa_ios_app`  
- prd → `bundle exec fastlane prd_ios_app`  