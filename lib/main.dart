import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app/app_module.dart';
import 'config/app_config.dart';
import 'notifications/notifications_service.dart';

void main({String environment = "dev"}) async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  var environmentJson = await _getEnvironmentJson(environment);
  var appConfig = AppConfig.fromJson(environmentJson);
  var remoteConfig = await setupRemoteConfig(appConfig);

  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);

  await NotificationsService.startNotificationsListener();

  await lockRotation();

  runZoned(() {
    runApp(
      ModularApp(
        module: AppModule(
          environmentJson: environmentJson,
          remoteConfig: remoteConfig,
        ),
      ),
    );
  }, onError: FirebaseCrashlytics.instance.recordError);
}

Future<Map<String, dynamic>> _getEnvironmentJson(String environment) async {
  final content = await rootBundle.loadString('configs/$environment.json');
  return jsonDecode(content);
}

Future<RemoteConfig> setupRemoteConfig(AppConfig appConfig) async {
  final remoteConfig = await RemoteConfig.instance;
  final defaults = {'exampleRemoteKey': 'exampleRemoteConfigValue'};
  await remoteConfig.setDefaults(defaults);
  await remoteConfig.fetchAndActivate();
  return remoteConfig;
}

void lockRotation() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}
