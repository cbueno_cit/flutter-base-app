import 'package:flutter/material.dart';

import 'colors.dart';
import 'fonts.dart';

class BaseTheme {
  static ThemeData buildTheme() {
    var base = ThemeData.light();
    return base.copyWith(
      textTheme: _baseTextTheme(base.textTheme),
      iconTheme: _baseIconTheme(base.iconTheme),
    );
  }

  static TextTheme _baseTextTheme(TextTheme base) {
    return base.copyWith(
      headline6: base.headline6.copyWith(
        fontFamily: Fonts.robotoRegular,
        fontSize: 24,
        color: CustomColors.marineBlue,
        fontWeight: FontWeight.bold,
      ),
      subtitle1: base.subtitle1.copyWith(
        fontFamily: Fonts.robotoRegular,
        fontSize: 12.0,
        color: CustomColors.marineBlue,
        fontWeight: FontWeight.normal,
      ),
      bodyText1: base.bodyText1.copyWith(
          fontFamily: Fonts.robotoRegular,
          color: Colors.white,
          fontSize: 12,
          height: 1.4),
      headline4: base.headline4.copyWith(
        fontFamily: Fonts.robotoLight,
        fontSize: 24,
        color: Colors.black,
        fontWeight: FontWeight.w300,
      ),
      headline3: base.headline3.copyWith(
        fontFamily: Fonts.robotoRegular,
        color: CustomColors.marineBlue,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      headline2: base.headline2.copyWith(
          fontFamily: Fonts.robotoRegular,
          fontWeight: FontWeight.bold,
          color: Colors.white,
          fontSize: 18),
      headline1: base.headline1.copyWith(
        fontFamily: Fonts.robotoRegular,
        fontSize: 16.0,
        color: Colors.white,
        fontWeight: FontWeight.normal,
      ),
      button: base.button.copyWith(
        fontFamily: Fonts.robotoRegular,
        fontSize: 14,
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
      caption: base.caption.copyWith(
        fontFamily: Fonts.robotoRegular,
        fontSize: 16,
        color: CustomColors.silver,
        fontWeight: FontWeight.normal,
      ),
    );
  }

  static IconThemeData _baseIconTheme(IconThemeData base) {
    return base.copyWith(
      color: CustomColors.marineBlue,
    );
  }
}
