import 'package:flutter/material.dart';

class CustomColors {
  static const marineBlue = Color.fromARGB(255, 0, 45, 114);
  static const cerulean = Color.fromARGB(255, 0, 119, 200);
  static const ceruleanTwo = Color.fromARGB(255, 0, 122, 201);
  static const ceruleanThree = Color.fromARGB(255, 0, 128, 204);
  static const silver = Color.fromARGB(255, 194, 201, 203);
  static const paleGrey = Color.fromARGB(255, 231, 234, 236);
  static const paleGreyTwo = Color.fromARGB(255, 243, 244, 245);
  static const paleGreyThree = Color.fromARGB(255, 244, 246, 248);
  static const greyish = Color.fromARGB(255, 163, 163, 163);
  static const greyishBrown = Color.fromARGB(255, 86, 86, 86);
  static const sapGreen = Color.fromARGB(255, 101, 141, 27);
}
