class Fonts {
  static const String whitneySemibold = 'Whitney-Semibold';
  static const String robotoRegular = 'Roboto-Regular';
  static const String robotoLight = 'Roboto-Light';
  static const String robotoMedium = 'Roboto-Medium';

}
