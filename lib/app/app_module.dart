import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../config/app_config.dart';
import 'app_routes.dart';
import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'modules/notification/notification_module.dart';
import 'shared/services/device_info/device_info_service.dart';

class AppModule extends MainModule {
  static Inject get to => Inject<AppModule>.of();

  final Map<String, dynamic> environmentJson;
  final RemoteConfig remoteConfig;

  AppModule({@required this.environmentJson, @required this.remoteConfig});

  @override
  List<Bind> get binds => [
        Bind((i) => AppConfig.fromJson(environmentJson)),
        Bind((i) => DeviceInfoService()),
        Bind((i) => FirebaseAnalytics()),
        Bind((i) => remoteConfig),
      ];

  @override
  Widget get bootstrap => AppWidget();

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Routes.home, module: HomeModule()),
        ModularRouter(Routes.notifications, module: NotificationModule()),
      ];
}
