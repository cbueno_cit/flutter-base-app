import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../config/app_config.dart';
import 'pages/home/home_controller.dart';
import 'pages/home/home_page.dart';
import 'services/features_service.dart';

class HomeModule extends ChildModule {
  static Inject get to => Inject<HomeModule>.of();

  @override
  List<Bind> get binds => [
        // Services
        Bind((i) => FeaturesService(
              appConfig: Modular.get<AppConfig>(),
              remoteConfig: Modular.get<RemoteConfig>(),
            )),

        // Controllers
        Bind((i) => HomeController(
              appConfig: Modular.get<AppConfig>(),
              remoteConfig: Modular.get<RemoteConfig>(),
              examplesService: Modular.get<FeaturesService>(),
            )),
      ];

  @override
  List<ModularRouter> get routers => [ModularRouter(Modular.initialRoute, child: (_, __) => HomePage())];
}
