import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';

import '../../../../config/app_config.dart';

class FeaturesService {
  final AppConfig appConfig;
  final RemoteConfig remoteConfig;

  FeaturesService({@required this.appConfig, @required this.remoteConfig});

  Future<List<String>> getFeatures() async {
    await Future.delayed(Duration(seconds: 2));
    return <String>[
      'Device Information',
      'Environment segregation',
      'Fastlane to build and distribute application',
      'Firebase Analytics',
      'Firebase Auth',
      'Firebase Cloud Message to Push Notifications',
      'Firebase Crashlytics',
      'Firebase Distribution',
      'Firebase Performance',
      'Firebase Remote Config',
      'Flutter analyze hook',
      'Linter rules to Effective Dart guideline',
      'Notification page',
      'Themes',
    ];
  }
}
