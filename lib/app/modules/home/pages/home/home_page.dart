import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../app_routes.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;

  HomePage({this.title = 'Flutter Base App'});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  @override
  void initState() {
    controller.getFeatures();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(24),
              child: Text(
                'Running on ${controller.appConfig.environment.toUpperCase()} environment',
                style: TextStyle(fontSize: 24),
              ),
            ),
            Observer(
              builder: (_) {
                if (controller.features.value == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (controller.features.error != null) {
                  return Center(
                    child: RaisedButton(
                      child: Text('Ocorreu um erro, tente novamente'),
                      onPressed: controller.getFeatures,
                    ),
                  );
                }
                final features = controller.features.value;
                return Expanded(
                  child: ListView.builder(
                    itemCount: features.length,
                    itemBuilder: (_, index) {
                      final feature = features[index];
                      return ListTile(
                        title: Text(
                          feature,
                          style: TextStyle(fontSize: 16),
                        ),
                        onTap: () => Modular.to.pushNamed(Routes.featureDetails),
                      );
                    },
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
