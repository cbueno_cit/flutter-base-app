import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';

import '../../../../../config/app_config.dart';
import '../../services/features_service.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final AppConfig appConfig;
  final RemoteConfig remoteConfig;
  final FeaturesService examplesService;

  _HomeControllerBase({
    @required this.appConfig,
    @required this.remoteConfig,
    @required this.examplesService,
  });

  @observable
  ObservableFuture<List<String>> features;

  @action
  Future getFeatures() async {
    features = examplesService.getFeatures().asObservable();
  }
}
