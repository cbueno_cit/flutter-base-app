// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$featuresAtom = Atom(name: '_HomeControllerBase.features');

  @override
  ObservableFuture<List<String>> get features {
    _$featuresAtom.reportRead();
    return super.features;
  }

  @override
  set features(ObservableFuture<List<String>> value) {
    _$featuresAtom.reportWrite(value, super.features, () {
      super.features = value;
    });
  }

  final _$getFeaturesAsyncAction =
      AsyncAction('_HomeControllerBase.getFeatures');

  @override
  Future<dynamic> getFeatures() {
    return _$getFeaturesAsyncAction.run(() => super.getFeatures());
  }

  @override
  String toString() {
    return '''
features: ${features}
    ''';
  }
}
