import 'dart:convert';

import 'package:flutter/material.dart';

import '../../../themes/colors.dart';

class NotificationPage extends StatelessWidget {
  final String title = "Notificações";
  final dynamic data;

  NotificationPage({this.data});

  @override
  Widget build(BuildContext context) {
    final jsonData = jsonDecode(data);
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        color: CustomColors.paleGreyThree,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    jsonData['title'],
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontSize: 32,
                        ),
                  ),
                ),
                Image.network(
                  jsonData['image_url'],
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    jsonData['content'],
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontSize: 20,
                        ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
