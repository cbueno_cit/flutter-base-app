import 'package:flutter_modular/flutter_modular.dart';

import 'notification_page.dart';

class NotificationModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers =>
      [ModularRouter(Modular.initialRoute, child: (_, args) => NotificationPage(data: args.data))];

  static Inject get to => Inject<NotificationModule>.of();
}
