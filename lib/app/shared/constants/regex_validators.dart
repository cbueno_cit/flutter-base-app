class RegexValidators {
  static const oneWord = "[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚ]";
  static const words = "[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚ ]";
  static const emailValidation = r"^[a-z0-9.a-z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-z0-9]+\.[a-z]+";
  static const emailTyping = "[a-z0-9@.-_]";
  static const onlyNumbers = "[0-9]";
  static const fullNamePattern = r'''[A-Za-zÀ-Ÿà-ÿ][A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-Za-zÀ-Ÿà-ÿ][A-zÀ-ÿ']+$''';
  static const onlyLettersAndNumbers = r'''[a-zA-Z0-9]''';
}