class InputMasks{
  static const cpf = "###.###.###-##";
  static const phone = "(##) ####-####";
  static const mobile = "(##) #####-####";
  static const dateddMMyyyy = "##/##/####";
  static const cep = "#####-###";
}