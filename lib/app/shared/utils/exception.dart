import 'package:flutter/services.dart';

class BaseException <T extends Exception> {
  dynamic ex;

  BaseException(this.ex);

  @override
  String toString() {
    switch (ex.runtimeType) {
      case PlatformException:
          return "Exception: ${ex.message}";
      default:
        return ex.toString();
    }
  }
}
