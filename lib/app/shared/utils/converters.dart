/// Converters Utils Class
class Converters{
  
  /// Convertes String to Int Number
  static int stringToInt(String number) => 
    number == null ? null : int.parse(number);

  /// Converts String From Int Number
  static String stringFromInt(int number) => number?.toString();

  /// Capitalizes a String
  static String capitalize(String value) {
    if (value == null || value.isEmpty) {
      return value;
    }
    var lower = value.toLowerCase().trim();
    return '${lower[0].toUpperCase()}${lower.substring(1)}';
  } 
}