import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../themes/base_theme.dart';
import 'app_routes.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light
      ),
    );

    return MaterialApp(
      navigatorKey: Modular.navigatorKey,
      title: 'Flutter Base App',
      theme: BaseTheme.buildTheme(),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: Modular.get<FirebaseAnalytics>())
      ],
      initialRoute: Routes.home,
      onGenerateRoute: Modular.generateRoute,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('pt_BR'),
      ],
    );
  }
}
