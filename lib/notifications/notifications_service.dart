import 'dart:convert';
import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../app/app_routes.dart';
import 'enums/notification_type.dart';

mixin NotificationsService {
  static const _channelId = "flutter_base";
  static const _channelName = "Flutter Base";
  static const _channelDescription = "Flutter base notifications";
  static const _notificationsIconPath = "drawable/icon_push";

  static bool isOnLaunch;
  static String onLaunchData;

  static final _localNotifications = FlutterLocalNotificationsPlugin();
  static final _firebaseMessaging = FirebaseMessaging.instance;

  static Future<void> startNotificationsListener() async {
    setupFirebaseMessaging();
    await setupLocalNotifications();
  }

  static void setupFirebaseMessaging() {
    _firebaseMessaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    if (Platform.isIOS) {
      _firebaseMessaging.requestPermission(
        sound: true,
        badge: true,
        alert: true,
      );
    }

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      try {
        isOnLaunch = true;
        onLaunchData = getMessageEncoded(message.data);
      } catch (ex, stackTrace) {
        FirebaseCrashlytics.instance.recordError(ex, stackTrace, reason: 'Notification Callback Error: _eventOnLaunch');
      }
    });

    FirebaseMessaging.onMessage.listen((message) async {
      try {
        isOnLaunch = false;
        final androidNotification = AndroidNotificationDetails(_channelId, _channelName, _channelDescription,
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker',
            color: Color.fromRGBO(0, 76, 151, 0));

        var title = Platform.isIOS ? message.data['aps']['alert']['title'] : message.notification.title;

        var body = Platform.isIOS ? message.data['aps']['alert']['body'] : message.notification.body;

        final iosNotification = IOSNotificationDetails();

        final macOSNotification = MacOSNotificationDetails();

        await _localNotifications.show(
          0,
          title,
          body,
          NotificationDetails(android: androidNotification, iOS: iosNotification, macOS: macOSNotification),
          payload: getMessageEncoded(message.data),
        );
      } catch (ex, stackTrace) {
        FirebaseCrashlytics.instance
            .recordError(ex, stackTrace, reason: 'Notification Callback Error: _eventOnMessage');
      }
    });
  }

  static Future<void> setupLocalNotifications() async {
    Future<void> _eventOnSelectNotifications(message) async {
      isOnLaunch = false;
      await notificationHandler(message);
    }

    final androidSettings = AndroidInitializationSettings(_notificationsIconPath);
    final iosSettings = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    final macOSSettings = MacOSInitializationSettings();

    final settings = InitializationSettings(android: androidSettings, iOS: iosSettings, macOS: macOSSettings);

    const channel = AndroidNotificationChannel(
      _channelId,
      _channelName,
      _channelDescription,
      importance: Importance.max,
    );

    await _localNotifications
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await _localNotifications.initialize(
      settings,
      onSelectNotification: (message) async => _eventOnSelectNotifications(message),
    );
  }

  static Future<String> getDeviceToken() async {
    return await _firebaseMessaging.getToken();
  }

  static String getMessageEncoded(Map<String, dynamic> message) {
    return Platform.isIOS ? jsonEncode(message) : jsonEncode(message['data']);
  }

  static NotificationType getNotificationType(dynamic message) {
    try {
      final messageData = jsonDecode(message);
      final notificationTypeValue = messageData['notification_type'];
      return NotificationType.values[int.parse(notificationTypeValue)];
    } catch (_) {
      return NotificationType.none;
    }
  }

  static Future<void> notificationHandler(dynamic message) async {
    final notificationType = getNotificationType(message);
    switch (notificationType) {
      case NotificationType.none:
        break;
      case NotificationType.redirectToNofication:
        await Modular.to.pushNamed(
          Routes.notifications,
          arguments: message,
        );
        break;
    }
  }
}
