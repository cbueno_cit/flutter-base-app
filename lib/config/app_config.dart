import 'package:json_annotation/json_annotation.dart';

part 'app_config.g.dart';

@JsonSerializable()
class AppConfig {
  final String environment;

  AppConfig({
    this.environment
  });

  factory AppConfig.fromJson(Map<String, dynamic> json) {
    return _$AppConfigFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AppConfigToJson(this);
}