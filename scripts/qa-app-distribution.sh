echo "\n-=-=-= Running Android Distribution =-=-=-\n"
cd android/
bundle exec fastlane qa_android_app

echo "\n-=-=-= Running iOS Distribution =-=-=-\n"
cd ../ios/
bundle exec fastlane qa_ios_app