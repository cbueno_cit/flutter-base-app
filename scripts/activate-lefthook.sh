#!/bin/bash
#
# How to Execute:
# WINDOWS 10 CMD: bash activate-lefthook.sh
# UNIX: sh activate-lefthook.sh
#
# https://github.com/Arkweid/lefthook
# https://pub.dev/packages/lefthook
cd .
if command -v brew > /dev/null; then
    echo 'Installing Lefthook via Brew https://github.com/Arkweid/lefthook'
    brew install Arkweid/lefthook/lefthook
else
    echo 'Installing Lefthook via pub global https://pub.dev/packages/lefthook'
    pub global activate lefthook
fi

lefthook install -a